﻿using AutomationApp.AutoModel;
using Microsoft.Win32.TaskScheduler;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using Task = System.Threading.Tasks.Task;

namespace AutomationApp
{
    public partial class TestWindow : Form
    {
        private string _envToTest;
        private const string ENVCONFIG = "EnvironmentConfig.txt";
        public string[] envData { get; private set; }

        List<Task> tasks = new List<Task>();

        public Dictionary<string, StatusEventArgs> statuses;
        public string settingsfile = Environment.CurrentDirectory + "\\settings.ini";

        TestDirectory testDirectory;
        public TestWindow(bool automated)
        {
            /*if (!IsRunAsAdmin())
            {
                ProcessStartInfo proc = new ProcessStartInfo();
                proc.UseShellExecute = true;
                proc.WorkingDirectory = Environment.CurrentDirectory;
                proc.FileName = Assembly.GetEntryAssembly().CodeBase;
                proc.Verb = "runas";
                try
                {
                    Process.Start(proc);
                    Application.Exit();
                }
                catch
                {
                    //Log.Write("This application requires elevated credentials in order to operate correctly!"); }
                }
            }*/
            InitializeComponent();

            if (automated)
            {

                //this.Visible = false;

                notifyIcon.Icon = new System.Drawing.Icon(Path.GetFullPath(@"pie_chart_FSU_icon.ico"));
                notifyIcon.Visible = true;
                notifyIcon.BalloonTipTitle = "Automated Web Testing will start in 5 minutes.";
                notifyIcon.BalloonTipText = "Click Here to suspend testing";
                notifyIcon.ShowBalloonTip(100);
                Show();
                runAutomated().Wait();

                
            }
        }

        private async Task runAutomated()
        {
            //await Task.Delay(60 * 5 * 1000);
            //await Task.Delay(10000);

            XmlSerializer deserializer = new XmlSerializer(typeof(AutomatedConfig));
            using (var reader = new StreamReader(settingsfile))
            {
                var myConfig = (AutomatedConfig)deserializer.Deserialize(reader);
                PopulateComboBox();

                foreach (var script in Directory.GetDirectories(myConfig.scriptsFolder))
                {
                    directoryPath.Text = script;
                    browserBox.SelectedIndex = myConfig.browserIndex;
                    int envIndex = Convert.ToInt32(File.ReadAllLines(Path.Combine(script, ENVCONFIG))[0].Split('=')[1]);

                    _envToTest = File.ReadAllLines(Path.Combine(script, ENVCONFIG))[envIndex].Split('=')[1];
                    chkOldScreen.Checked = myConfig.oldscreenshotter;
                    chkDebug.Checked = myConfig.debugmode;

                    PopulateEnvironment();
                    env.SelectedIndex = envIndex - 1;

                    await runScript();
                }
            }
        }

        private void TestWindow_Load(object sender, EventArgs e)
        {

            if(browserBox.Items.Count == 0) PopulateComboBox();
            browserBox.SelectedIndex = 0;

            reportButton.Hide();
            groupBox.Hide();
            author.Text = Environment.UserName;
            directoryPath.Enabled = false;
            env.Enabled = false;

            IncludeTests.Checked = true;
        }

        private void PopulateComboBox()
        {
            browserBox.Items.Add("Chrome");
            browserBox.Items.Add("Firefox");
            browserBox.Items.Add("InternetExplorer");
            browserBox.Items.Add("MicrosoftEdge");
            browserBox.Items.Add("Headless-Chrome");
            //browserBox.Items.Add("Batch-Processing-Parallel");
            browserBox.Items.Add("Batch-Processing-Standard");
        }

        private void notifyIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Show();
            this.WindowState = FormWindowState.Normal;
            notifyIcon.Visible = false;
        }

        private bool IsRunAsAdmin()
        {
            WindowsIdentity id = WindowsIdentity.GetCurrent();
            WindowsPrincipal principal = new WindowsPrincipal(id);
            return principal.IsInRole(WindowsBuiltInRole.Administrator);
        }

        private void browse_Click(object sender, EventArgs e)
        {
            //Opens Dialog to browse Test Folder
            using (var browse = new FolderBrowserDialog())
            {
                browse.SelectedPath = @"G:\work\Automation Test Tool Regression Tests";
                DialogResult result = browse.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(browse.SelectedPath))
                {
                    directoryPath.Enabled = true;
                    env.Enabled = true;
                    directoryPath.Text = browse.SelectedPath;

                    PopulateEnvironment();
                }
            }
        } 

        private void runButton_Click(object sender, EventArgs e)
        {
            runScript();           
        }

        private async Task runScript()
        {
            statuses = new Dictionary<string, StatusEventArgs>();

            testDirectory = new TestDirectory(directoryPath.Text, _envToTest, IncludeTests.Checked, chkDebug.Checked, chkOldScreen.Checked);

            testDirectory.StatusChanged += (s, ex) =>
            {
                this.BeginInvoke(new System.Action(() =>
                {
                    int max = 0;
                    int current = 0;
                    statusLbl.Text = "";

                    if (statuses.Keys.Contains(ex.browser)) statuses[ex.browser] = ex;
                    else statuses.Add(ex.browser, ex);

                    foreach (var status in statuses.Values)
                    {
                        statusLbl.Text += status.browser + ": ";
                        if (status.status == "Running") statusLbl.Text += status.current + "/" + status.max + " steps" + " ";
                        else if (status.status == "Finished") statusLbl.Text += "Finished";
                        max += status.max;
                        current += status.current;
                    }

                    ProgressBar.Maximum = max;
                    ProgressBar.Value = current;
                }));

            };

            if (string.IsNullOrWhiteSpace(directoryPath.Text))
            {
                MessageBox.Show("Directory path is empty", "Directory Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            try
            {
                testDirectory.LoadFiles();
            }
            catch (DirectoryNotFoundException dirExc)
            {
                MessageBox.Show("Directory has no test files. " + dirExc.Message, "Directory Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            catch (Exception fileExc)
            {
                MessageBox.Show("Error while loading files. Please close all Test Files before running or Check the paths for test files.\n\n" + fileExc.Message, "File Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            try
            {
                var envx = ((KeyValuePair<string, string>)env.SelectedItem).Key;
                var selectedBrowser = browserBox.SelectedItem.ToString();
                var browsersToTest = new List<string>() { "Chrome", "Firefox" };

                if (selectedBrowser == "Batch-Processing-Parallel")
                {
                    await Task.WhenAll(new[]
                    {
                        Task.Run(() => testDirectory.Run("Chrome", envx, author.Text, directoryPath.Text)),
                        Task.Run(() => testDirectory.Run("Firefox", envx, author.Text, directoryPath.Text))
                    });
                }
                else if (selectedBrowser == "Batch-Processing-Standard")
                {
                    foreach (var browser in browsersToTest)
                    {
                        Task.Run(async () => await testDirectory.Run(browser, envx, author.Text, directoryPath.Text)).Wait();
                    }
                }
                else await Task.Run(() => testDirectory.Run(selectedBrowser, envx, author.Text, directoryPath.Text));
            }
            catch (Exception exeExc)
            {
                TopMost = true;
                MessageBox.Show("Error while executing Test Files. Unable to connect to the server or Execution stopped unexpectedly.\n" + exeExc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Close();
                return;
            }

            TopMost = true;
            ProgressBar.Maximum = 100;
            ProgressBar.Value = 100;
            statusLbl.Text = "Finished";

            try
            {
                testDirectory.TestReports.ForEach(n => System.Diagnostics.Process.Start(n));

                reportButton.Show();
                runButton.BackColor = Color.Gray;
                runButton.Enabled = false;

                if (deleteReports.Checked)
                {
                    int dayCount = 0;

                    if (int.TryParse(numericUpDown.Text, out dayCount))
                    {
                        DeleteReports(dayCount);
                    }
                    else
                    {
                        MessageBox.Show("Please enter valid number of days to delete reports.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch
            {
                MessageBox.Show("Error while generating report.", "Report Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Close();
                return;
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {

            Close();
        }

        //Populates the enviroment and its respective url from current directorie's EnvironmentConfig file.
        private void PopulateEnvironment()
        {
            Dictionary<string, string> envSource = new Dictionary<string, string>();    

            try
            {
                envData = File.ReadAllLines(Path.Combine(directoryPath.Text, ENVCONFIG));
            }
            catch
            {
                MessageBox.Show("Directory is invalid. EnvironmentConfig file is not found in directory.", "Invalid Directory Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            
            foreach (string envLine in envData)
            {
                if (envLine.StartsWith("AUTOMATE")) continue;
                try
                {
                    var envKey = envLine.Split('=')[0].Trim();
                    var envValue = envLine.Split('=')[1].Trim();
                    envSource.Add(envKey, envValue);
                }
                catch
                {
                    MessageBox.Show("EnvironmentConfig file is not valid. Please verify the file contents.", "Invalid File Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }             
            }

            env.DataSource = new BindingSource(envSource, null);
            env.DisplayMember = "Key";
            env.ValueMember = "Value";
            env.SelectedIndex = 0;
        }

        private void env_SelectedIndexChanged(object sender, EventArgs e)
        {
            _envToTest = ((KeyValuePair<string, string>)env.SelectedItem).Value;
            runButton.BackColor = Color.DarkSeaGreen;
        }

        private void reportButton_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(testDirectory.CurrentTestReport);
            Close();
        }

        //Deletes reports based on user input value from form.
        private void DeleteReports(int days)
        {
            var reports = new DirectoryInfo(testDirectory._testReportsFolder.FullName).GetDirectories().ToList();
            List<DirectoryInfo> reportsToDelete = new List<DirectoryInfo>();

            try
            {
                foreach (var report in reports)
                {
                    string[] reportNameValues = report.Name.Split('_');
                    DateTime reportDate = Convert.ToDateTime(reportNameValues[1] + "/" + reportNameValues[2] + "/" + reportNameValues[3]);

                    if (reportDate < DateTime.Now.AddDays( - + days))
                    {
                        reportsToDelete.Add(report);
                    }
                }
            }
            catch(Exception delExc)
            {
                MessageBox.Show("Error Deleting reports" + delExc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            DialogResult deleteRepose = MessageBox.Show(String.Format("There are \"{0}\" reports which are older than \"{1}\" days from today's date. Click \"Yes\" to delete reports or Click \"No\".", reportsToDelete.Count, days), "Respond" , MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (deleteRepose == DialogResult.Yes)
            {
                reportsToDelete.ForEach(r => Directory.Delete(r.FullName, true));
            }
        }

        private void deleteReports_CheckedChanged(object sender, EventArgs e)
        {
            if (deleteReports.Checked)
            {
                groupBox.Show();
                numericUpDown.Text = ConfigurationManager.AppSettings["DeleteReportRange"];
            }
            else
            {
                groupBox.Hide();
            }
        }

        private void saveSettings()
        {
            try
            {
                XmlSerializer xsSubmit = new XmlSerializer(typeof(AutomatedConfig));
                var xml = "";
                using (var sww = new StringWriter())
                {
                    using (XmlWriter writer = XmlWriter.Create(sww))
                    {
                        xsSubmit.Serialize(writer, new AutomatedConfig() {
                            activated = chkActivated.Checked,
                            time = timeSelect.Value,
                            scriptsFolder = textBoxFolder.Text,
                            browserIndex = browserBox.SelectedIndex,
                            oldscreenshotter = chkOldScreen.Checked,
                            debugmode = chkDebug.Checked
                        });
                        xml = sww.ToString();
                        File.WriteAllText(settingsfile, xml);
                    }
                }
            }
            catch
            {
                Console.WriteLine("Error while saving settings");
                MessageBox.Show("Error while saving settings", "Info", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            try
            {
                if (chkActivated.Checked)
                {
                    using (TaskService ts = new TaskService())
                    {
                        Microsoft.Win32.TaskScheduler.Task t = ts.GetTask("AscensionAuto");
                        if (t != null) ts.RootFolder.DeleteTask("AscensionAuto");

                        // Create a new task definition and assign properties
                        TaskDefinition td = ts.NewTask();
                        td.Principal.UserId = "SYSTEM";
                        td.RegistrationInfo.Description = "Automates Daily Testing for Selenium";
                        td.Principal.LogonType = TaskLogonType.InteractiveToken;

                        // Add a trigger that will fire the task at this time every other day
                        DailyTrigger dt = (DailyTrigger)td.Triggers.Add(new DailyTrigger(2));
                        dt.StartBoundary = timeSelect.Value;
                        dt.DaysInterval = 1;

                        // Add an action that will launch Notepad whenever the trigger fires
                        td.Actions.Add(new ExecAction(Environment.CurrentDirectory + "\\AutomationApp.exe", "-automated", null));

                        // Register the task in the root folder
                        const string taskName = "AscensionAuto";
                        ts.RootFolder.RegisterTaskDefinition(taskName, td);

                        MessageBox.Show("Succesfully enabled daily Task", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    using (TaskService ts = new TaskService())
                    {
                        Microsoft.Win32.TaskScheduler.Task t = ts.GetTask("AscensionAuto");
                        if (t != null) ts.RootFolder.DeleteTask("AscensionAuto");

                        MessageBox.Show("Succesfully disabled daily Task", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                }
            }
            catch
            {
                Console.WriteLine("Error while adding daily task");
                MessageBox.Show("Error while adding daily task", "Info", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
        }

        private void btnSaveSettings_Click(object sender, EventArgs e)
        {
            saveSettings();
        }

        private void btnBrowseFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderDlg = new FolderBrowserDialog();
            folderDlg.ShowNewFolderButton = false;

            DialogResult result = folderDlg.ShowDialog();

            if (result == DialogResult.OK)
            {
                

                List<string> noenvconfig = new List<string>();
                //List<string> noenvautomate = new List<string>();
                foreach (var folder in Directory.GetDirectories(folderDlg.SelectedPath))
                {
                    var files = Directory.GetFiles(folder).Select(x=>Path.GetFileName(x)).ToList();

                    if (!files.Contains(ENVCONFIG))
                    {
                        noenvconfig.Add(new DirectoryInfo(folder).Name);
                        continue;
                    }

                    /*
                    var envFile = File.ReadAllLines(Path.Combine(folder, ENVCONFIG));
                    if (!envFile[0].StartsWith("AUTOMATE"))
                    {
                        //noenvautomate.Add(new DirectoryInfo(folder).Name);


                    }*/
                        
                }
                if (noenvconfig.Count > 0)
                {
                    string errorStr = "Invalid files in folder - " + Environment.NewLine;
                    foreach (var error in noenvconfig) errorStr += "- " + error + Environment.NewLine;
                    errorStr += "Please make sure all folders are Script Folders.";

                    MessageBox.Show(errorStr, "Info", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }/*
                if (noenvautomate.Count > 0)
                {
                    string errorStr = "Automation Environment not set for - " + Environment.NewLine;
                    foreach (var error in noenvautomate) errorStr += "- " + error + Environment.NewLine;
                    errorStr += "Please make sure all folders have automation environment set.";

                    MessageBox.Show(errorStr, "Info", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }*/
                var newFrm = new BatchScripts(folderDlg.SelectedPath);
                newFrm.Show();

                textBoxFolder.Text = folderDlg.SelectedPath;
                Environment.SpecialFolder root = folderDlg.RootFolder;

                MessageBox.Show("All folders are ok. Select required environment to auto run in the new window.","Info",MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            foreach (var script in Directory.GetDirectories(textBoxFolder.Text))
            {
                directoryPath.Text = script;
                int envIndex = Convert.ToInt32(File.ReadAllLines(Path.Combine(script, ENVCONFIG))[0].Split('=')[1]);
                if (envIndex == 0)
                    continue;

                _envToTest = File.ReadAllLines(Path.Combine(script, ENVCONFIG))[envIndex].Split('=')[1];

                PopulateEnvironment();
                env.SelectedIndex = envIndex - 1;

                await runScript();
            }
        }
    }
}
