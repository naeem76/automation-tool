﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationApp.Exceptions
{
    public class AutomationAppException : Exception
    {
        public AutomationAppException() : base()
        {

        }

        public AutomationAppException(string message) : base(message)
        {

        }

        public AutomationAppException(string message, Exception innerException) : base(message, innerException)
        {

        }
    }
}
