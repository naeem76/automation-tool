﻿namespace AutomationApp
{
    partial class TestWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.FolderBrowserDialog BrowseDirectory;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TestWindow));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.directoryPath = new System.Windows.Forms.TextBox();
            this.browse = new System.Windows.Forms.Button();
            this.runButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.env = new System.Windows.Forms.ComboBox();
            this.Browser = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.browserBox = new System.Windows.Forms.ComboBox();
            this.author = new System.Windows.Forms.TextBox();
            this.reportButton = new System.Windows.Forms.Button();
            this.ProgressBar = new System.Windows.Forms.ProgressBar();
            this.IncludeTests = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.statusLbl = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.deleteReports = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox = new System.Windows.Forms.GroupBox();
            this.numericUpDown = new System.Windows.Forms.NumericUpDown();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkDebug = new System.Windows.Forms.CheckBox();
            this.chkOldScreen = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.btnBrowseFolder = new System.Windows.Forms.Button();
            this.textBoxFolder = new System.Windows.Forms.TextBox();
            this.btnSaveSettings = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.timeSelect = new System.Windows.Forms.DateTimePicker();
            this.chkActivated = new System.Windows.Forms.CheckBox();
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            BrowseDirectory = new System.Windows.Forms.FolderBrowserDialog();
            this.groupBox1.SuspendLayout();
            this.groupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.900001F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(42, 190);
            this.label1.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 19);
            this.label1.TabIndex = 3;
            this.label1.Text = "Environment :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.900001F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(42, 117);
            this.label2.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(124, 19);
            this.label2.TabIndex = 4;
            this.label2.Text = "Directory Path :";
            // 
            // directoryPath
            // 
            this.directoryPath.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.directoryPath.Cursor = System.Windows.Forms.Cursors.Default;
            this.directoryPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.directoryPath.Location = new System.Drawing.Point(204, 116);
            this.directoryPath.Margin = new System.Windows.Forms.Padding(1);
            this.directoryPath.Name = "directoryPath";
            this.directoryPath.Size = new System.Drawing.Size(458, 21);
            this.directoryPath.TabIndex = 6;
            // 
            // browse
            // 
            this.browse.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.browse.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.browse.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.900001F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.browse.Location = new System.Drawing.Point(682, 108);
            this.browse.Margin = new System.Windows.Forms.Padding(1);
            this.browse.Name = "browse";
            this.browse.Size = new System.Drawing.Size(94, 34);
            this.browse.TabIndex = 9;
            this.browse.Text = "Browse";
            this.browse.UseVisualStyleBackColor = false;
            this.browse.Click += new System.EventHandler(this.browse_Click);
            // 
            // runButton
            // 
            this.runButton.BackColor = System.Drawing.SystemColors.ControlDark;
            this.runButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.runButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.900001F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.runButton.Location = new System.Drawing.Point(64, 409);
            this.runButton.Margin = new System.Windows.Forms.Padding(1);
            this.runButton.Name = "runButton";
            this.runButton.Size = new System.Drawing.Size(133, 42);
            this.runButton.TabIndex = 10;
            this.runButton.Text = "Run";
            this.runButton.UseVisualStyleBackColor = false;
            this.runButton.Click += new System.EventHandler(this.runButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.BackColor = System.Drawing.SystemColors.ControlDark;
            this.cancelButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cancelButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.900001F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelButton.Location = new System.Drawing.Point(229, 409);
            this.cancelButton.Margin = new System.Windows.Forms.Padding(1);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(127, 42);
            this.cancelButton.TabIndex = 11;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = false;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // env
            // 
            this.env.BackColor = System.Drawing.SystemColors.Window;
            this.env.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.env.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.env.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.900001F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.env.FormattingEnabled = true;
            this.env.Location = new System.Drawing.Point(204, 188);
            this.env.Margin = new System.Windows.Forms.Padding(1);
            this.env.Name = "env";
            this.env.Size = new System.Drawing.Size(201, 24);
            this.env.TabIndex = 12;
            this.env.SelectedIndexChanged += new System.EventHandler(this.env_SelectedIndexChanged);
            // 
            // Browser
            // 
            this.Browser.AutoSize = true;
            this.Browser.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Browser.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Browser.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.900001F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Browser.Location = new System.Drawing.Point(42, 250);
            this.Browser.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.Browser.Name = "Browser";
            this.Browser.Size = new System.Drawing.Size(78, 19);
            this.Browser.TabIndex = 14;
            this.Browser.Text = "Browser :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.900001F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(41, 316);
            this.label3.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 19);
            this.label3.TabIndex = 16;
            this.label3.Text = "Test Author :";
            // 
            // browserBox
            // 
            this.browserBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.browserBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.browserBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.900001F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.browserBox.Location = new System.Drawing.Point(204, 250);
            this.browserBox.Margin = new System.Windows.Forms.Padding(1);
            this.browserBox.Name = "browserBox";
            this.browserBox.Size = new System.Drawing.Size(201, 24);
            this.browserBox.TabIndex = 18;
            // 
            // author
            // 
            this.author.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.900001F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.author.Location = new System.Drawing.Point(204, 316);
            this.author.Margin = new System.Windows.Forms.Padding(1);
            this.author.Name = "author";
            this.author.Size = new System.Drawing.Size(201, 22);
            this.author.TabIndex = 17;
            // 
            // reportButton
            // 
            this.reportButton.BackColor = System.Drawing.Color.SeaShell;
            this.reportButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.reportButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reportButton.Location = new System.Drawing.Point(236, 83);
            this.reportButton.Margin = new System.Windows.Forms.Padding(1);
            this.reportButton.Name = "reportButton";
            this.reportButton.Size = new System.Drawing.Size(252, 34);
            this.reportButton.TabIndex = 19;
            this.reportButton.Text = "Click here for Test Report";
            this.reportButton.UseVisualStyleBackColor = false;
            this.reportButton.Click += new System.EventHandler(this.reportButton_Click);
            // 
            // ProgressBar
            // 
            this.ProgressBar.ForeColor = System.Drawing.Color.PaleGreen;
            this.ProgressBar.Location = new System.Drawing.Point(184, 29);
            this.ProgressBar.Margin = new System.Windows.Forms.Padding(1);
            this.ProgressBar.Name = "ProgressBar";
            this.ProgressBar.Size = new System.Drawing.Size(477, 12);
            this.ProgressBar.TabIndex = 21;
            // 
            // IncludeTests
            // 
            this.IncludeTests.AutoSize = true;
            this.IncludeTests.BackColor = System.Drawing.Color.White;
            this.IncludeTests.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.IncludeTests.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IncludeTests.Location = new System.Drawing.Point(474, 191);
            this.IncludeTests.Margin = new System.Windows.Forms.Padding(1);
            this.IncludeTests.Name = "IncludeTests";
            this.IncludeTests.Size = new System.Drawing.Size(146, 20);
            this.IncludeTests.TabIndex = 22;
            this.IncludeTests.Text = "Include Sub Tests";
            this.IncludeTests.UseVisualStyleBackColor = false;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Azure;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(42, 16);
            this.label4.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(731, 31);
            this.label4.TabIndex = 23;
            this.label4.Text = "Test Automation App";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.900001F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(19, 29);
            this.label5.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(98, 17);
            this.label5.TabIndex = 24;
            this.label5.Text = "Run Status :";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.groupBox1.Controls.Add(this.statusLbl);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.ProgressBar);
            this.groupBox1.Controls.Add(this.reportButton);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox1.Location = new System.Drawing.Point(42, 511);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(1);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(1);
            this.groupBox1.Size = new System.Drawing.Size(699, 134);
            this.groupBox1.TabIndex = 25;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Run Results";
            // 
            // statusLbl
            // 
            this.statusLbl.AutoSize = true;
            this.statusLbl.Location = new System.Drawing.Point(184, 48);
            this.statusLbl.Name = "statusLbl";
            this.statusLbl.Size = new System.Drawing.Size(0, 13);
            this.statusLbl.TabIndex = 26;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.900001F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(16, 93);
            this.label6.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(104, 17);
            this.label6.TabIndex = 25;
            this.label6.Text = "Test Report :";
            // 
            // deleteReports
            // 
            this.deleteReports.AutoSize = true;
            this.deleteReports.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteReports.Location = new System.Drawing.Point(474, 213);
            this.deleteReports.Margin = new System.Windows.Forms.Padding(1);
            this.deleteReports.Name = "deleteReports";
            this.deleteReports.Size = new System.Drawing.Size(153, 19);
            this.deleteReports.TabIndex = 27;
            this.deleteReports.Text = "Delete Test Reports";
            this.deleteReports.UseVisualStyleBackColor = true;
            this.deleteReports.CheckedChanged += new System.EventHandler(this.deleteReports_CheckedChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(4, 18);
            this.label7.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(123, 13);
            this.label7.TabIndex = 28;
            this.label7.Text = "Delete reports older than";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(183, 18);
            this.label8.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 13);
            this.label8.TabIndex = 30;
            this.label8.Text = "days. ";
            // 
            // groupBox
            // 
            this.groupBox.BackColor = System.Drawing.Color.Gainsboro;
            this.groupBox.Controls.Add(this.numericUpDown);
            this.groupBox.Controls.Add(this.label7);
            this.groupBox.Controls.Add(this.label8);
            this.groupBox.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.groupBox.Location = new System.Drawing.Point(474, 232);
            this.groupBox.Margin = new System.Windows.Forms.Padding(1);
            this.groupBox.Name = "groupBox";
            this.groupBox.Padding = new System.Windows.Forms.Padding(1);
            this.groupBox.Size = new System.Drawing.Size(219, 42);
            this.groupBox.TabIndex = 31;
            this.groupBox.TabStop = false;
            // 
            // numericUpDown
            // 
            this.numericUpDown.Location = new System.Drawing.Point(133, 15);
            this.numericUpDown.Margin = new System.Windows.Forms.Padding(1);
            this.numericUpDown.Name = "numericUpDown";
            this.numericUpDown.Size = new System.Drawing.Size(45, 20);
            this.numericUpDown.TabIndex = 32;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chkDebug);
            this.groupBox2.Controls.Add(this.chkOldScreen);
            this.groupBox2.Location = new System.Drawing.Point(459, 290);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(282, 79);
            this.groupBox2.TabIndex = 32;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Dev Tools";
            // 
            // chkDebug
            // 
            this.chkDebug.AutoSize = true;
            this.chkDebug.Enabled = false;
            this.chkDebug.Location = new System.Drawing.Point(7, 44);
            this.chkDebug.Name = "chkDebug";
            this.chkDebug.Size = new System.Drawing.Size(88, 17);
            this.chkDebug.TabIndex = 1;
            this.chkDebug.Text = "Debug Mode";
            this.chkDebug.UseVisualStyleBackColor = true;
            // 
            // chkOldScreen
            // 
            this.chkOldScreen.AutoSize = true;
            this.chkOldScreen.Checked = true;
            this.chkOldScreen.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkOldScreen.Enabled = false;
            this.chkOldScreen.Location = new System.Drawing.Point(7, 20);
            this.chkOldScreen.Name = "chkOldScreen";
            this.chkOldScreen.Size = new System.Drawing.Size(133, 17);
            this.chkOldScreen.TabIndex = 0;
            this.chkOldScreen.Text = "Use Old Screenshotter";
            this.chkOldScreen.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.button1);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.btnBrowseFolder);
            this.groupBox3.Controls.Add(this.textBoxFolder);
            this.groupBox3.Controls.Add(this.btnSaveSettings);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.timeSelect);
            this.groupBox3.Controls.Add(this.chkActivated);
            this.groupBox3.Location = new System.Drawing.Point(459, 376);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(282, 131);
            this.groupBox3.TabIndex = 33;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Automated Timing";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(203, 86);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = "Run";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(15, 114);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(223, 13);
            this.label11.TabIndex = 7;
            this.label11.Text = "Please select Automation Browser from above";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 62);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(110, 13);
            this.label10.TabIndex = 6;
            this.label10.Text = "Choose Scripts Folder";
            // 
            // btnBrowseFolder
            // 
            this.btnBrowseFolder.Location = new System.Drawing.Point(225, 58);
            this.btnBrowseFolder.Name = "btnBrowseFolder";
            this.btnBrowseFolder.Size = new System.Drawing.Size(53, 22);
            this.btnBrowseFolder.TabIndex = 5;
            this.btnBrowseFolder.Text = "Browse";
            this.btnBrowseFolder.UseVisualStyleBackColor = true;
            this.btnBrowseFolder.Click += new System.EventHandler(this.btnBrowseFolder_Click);
            // 
            // textBoxFolder
            // 
            this.textBoxFolder.Location = new System.Drawing.Point(125, 59);
            this.textBoxFolder.Name = "textBoxFolder";
            this.textBoxFolder.Size = new System.Drawing.Size(97, 20);
            this.textBoxFolder.TabIndex = 4;
            // 
            // btnSaveSettings
            // 
            this.btnSaveSettings.Enabled = false;
            this.btnSaveSettings.Location = new System.Drawing.Point(125, 86);
            this.btnSaveSettings.Name = "btnSaveSettings";
            this.btnSaveSettings.Size = new System.Drawing.Size(78, 23);
            this.btnSaveSettings.TabIndex = 3;
            this.btnSaveSettings.Text = "Save";
            this.btnSaveSettings.UseVisualStyleBackColor = true;
            this.btnSaveSettings.Click += new System.EventHandler(this.btnSaveSettings_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Enabled = false;
            this.label9.Location = new System.Drawing.Point(12, 32);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(95, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Choose Daily Time";
            // 
            // timeSelect
            // 
            this.timeSelect.Enabled = false;
            this.timeSelect.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.timeSelect.Location = new System.Drawing.Point(126, 29);
            this.timeSelect.Name = "timeSelect";
            this.timeSelect.ShowUpDown = true;
            this.timeSelect.Size = new System.Drawing.Size(96, 20);
            this.timeSelect.TabIndex = 1;
            // 
            // chkActivated
            // 
            this.chkActivated.AutoSize = true;
            this.chkActivated.Enabled = false;
            this.chkActivated.Location = new System.Drawing.Point(18, 91);
            this.chkActivated.Name = "chkActivated";
            this.chkActivated.Size = new System.Drawing.Size(71, 17);
            this.chkActivated.TabIndex = 0;
            this.chkActivated.Text = "Activated";
            this.chkActivated.UseVisualStyleBackColor = true;
            // 
            // notifyIcon
            // 
            this.notifyIcon.Text = "notifyIcon1";
            this.notifyIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon_MouseDoubleClick);
            // 
            // TestWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.LightGray;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(791, 686);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox);
            this.Controls.Add(this.deleteReports);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.IncludeTests);
            this.Controls.Add(this.author);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.browserBox);
            this.Controls.Add(this.Browser);
            this.Controls.Add(this.env);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.runButton);
            this.Controls.Add(this.browse);
            this.Controls.Add(this.directoryPath);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(1);
            this.MinimizeBox = false;
            this.Name = "TestWindow";
            this.Padding = new System.Windows.Forms.Padding(0, 0, 26, 50);
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Automation App";
            this.TransparencyKey = System.Drawing.Color.Silver;
            this.Load += new System.EventHandler(this.TestWindow_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox.ResumeLayout(false);
            this.groupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox directoryPath;
        private System.Windows.Forms.Button browse;
        private System.Windows.Forms.Button runButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.ComboBox env;
        private System.Windows.Forms.Label Browser;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox browserBox;
        private System.Windows.Forms.TextBox author;
        private System.Windows.Forms.Button reportButton;
        private System.Windows.Forms.ProgressBar ProgressBar;
        private System.Windows.Forms.CheckBox IncludeTests;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox deleteReports;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox;
        private System.Windows.Forms.NumericUpDown numericUpDown;
        private System.Windows.Forms.Label statusLbl;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox chkDebug;
        private System.Windows.Forms.CheckBox chkOldScreen;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnSaveSettings;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker timeSelect;
        private System.Windows.Forms.CheckBox chkActivated;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnBrowseFolder;
        private System.Windows.Forms.TextBox textBoxFolder;
        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button button1;
    }
}

