﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using ExcelDataReader;
using System.IO;

namespace AutomationApp.DataGenerator
{
    public class ReadExcelData
    {
        private List<Datacollection> DataCol = new List<Datacollection>();
        private void ClearData()
        {
           DataCol.Clear();
        }
        public DataTable ExcelToDataTable(string baseFile, string sheetName)
        {
            // Open the provided file and store in the form of stream
            using (var stream = File.Open(baseFile, FileMode.Open, FileAccess.Read))
            {
                using (var reader = ExcelReaderFactory.CreateOpenXmlReader(stream))
                {
                    var result = reader.AsDataSet(new ExcelDataSetConfiguration()
                    {
                        ConfigureDataTable = (data) => new ExcelDataTableConfiguration()
                        {
                            UseHeaderRow = true
                        }
                    });
                    //Getting the tables along with values
                    var table = result.Tables;
                    // Storing the tables result in data table
                    var resultTable = table[sheetName];
                    return resultTable;
                }
            }
        }

        public string ReadTestStep(int rowNumber, string columnName)
        {
            try
            {
                //Uses LINQ library for data retrieval
                rowNumber = rowNumber - 1;
                var data = (from colData in DataCol
                            where (colData.colName == columnName) && (colData.rowNumber == rowNumber)
                            select colData.colValue).SingleOrDefault();
                return data;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception from ExcelDataGenerator class" + Environment.NewLine +
                                  e.Message);
                return null;
            }
        }

        public void PopulateInCollection(string baseFile,string sheetName)
        {
            ClearData();
            var table = ExcelToDataTable(baseFile, sheetName);
            //Iteration of rows and columns
            for (var row = 1; row <= table.Rows.Count; row++)
                for (var col = 0; col < table.Columns.Count; col++)
                {
                    var dtTable = new Datacollection
                    {
                        rowNumber = row,
                        colName = table.Columns[col].ColumnName.ToLower().Replace(" ",""),
                        colValue = table.Rows[row - 1][col].ToString().Trim()
                    };
                    // Adding row values
                    DataCol.Add(dtTable);
                }
        }

        public class Datacollection
        {
            public int rowNumber { get; set; }
            public string colName { get; set; }
            public string colValue { get; set; }
        }
    }
}
