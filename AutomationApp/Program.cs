﻿using System;
using System.Windows.Forms;

namespace AutomationApp
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            
            String[] arguments = Environment.GetCommandLineArgs();
            if (arguments.Length > 1)
            {
                if (arguments[1] == "-automated")
                {
                    TestWindow mainWindow = new TestWindow(true);
                    mainWindow.Visible = false;
                    Application.Run();
                }
            }
            else Application.Run(new TestWindow(false));

        }              
    }
}
