﻿using AutomationApp.ExecutorModel;
using AventStack.ExtentReports;
using AventStack.ExtentReports.MarkupUtils;
using System;

namespace AutomationApp
{
    public class TestAction
    {
        private IMarkup _markup;
        private ExtentTest _log;
        public string Keyword { get; set; }
        public string LocatorType { get; set; }
        public string LocatorValue { get; set; }
        public string Value { get; set; }
        public string TestStep { get; set; }
        public bool BreakOnFail { get; set; }
        public string ActionExceptionMessage { get; set; }
        public bool ActionSucceeded { get; private set; }
        public bool ActionStoppedTest { get; private set; }
        public string ReportScreenShot { get; private set; }
        private string _actionDuration;
        private bool _includeSubTests;

        //Sets the values by getting from each test file.
        public TestAction(string keyword, string locatorType, string locatorValue, string value, string testStep, bool breakOnFail, bool includeSubTests)
        {
            Keyword = keyword;
            LocatorType = locatorType;
            LocatorValue = locatorValue;
            Value = value;
            TestStep = testStep;
            BreakOnFail = breakOnFail;
            ActionSucceeded = false;
            ActionStoppedTest = false;
            _includeSubTests = includeSubTests;
        }
        
        //Triggers the action executor methods to perfrom operations on the web page.
        public bool RunAction(ActionExecutor actionExecutor, ExtentTest log)
        {
            _log = log;

            DateTime startTime = DateTime.Now;
            var actionResult = actionExecutor.PerformAction(Keyword, LocatorType, LocatorValue, Value, TestStep);
            DateTime endTime = DateTime.Now;

            TimeSpan timespan = (endTime - startTime);
            _actionDuration = String.Format("{0} hr, {1} min, {2} sec, {3} ms", timespan.Hours, timespan.Minutes, timespan.Seconds, timespan.Milliseconds);

            ActionSucceeded = actionResult.ActionStatus;
            ReportScreenShot = actionResult.Screenshot;
            ActionExceptionMessage = actionResult.ActionException;

            if (!ActionSucceeded && BreakOnFail)
            {
                ActionStoppedTest = true;
                LogAsBreakOnFail();
                return ActionStoppedTest;
            }

            if (ActionSucceeded && !ActionStoppedTest)
            {    
                LogAsPassed();
            }
            else if (!ActionSucceeded && !ActionStoppedTest)
            {
                LogAsFail();
            }

            return ActionStoppedTest;
        }

        public void LogAsPassed()
        {
            if (Keyword.ToLower().Contains("testcase"))
            {
                _markup = MarkupHelper.CreateLabel(TestStep + " -- TEST CASE", ExtentColor.Grey);
                _log.Info(_markup);
            }
            else if (Keyword.ToLower() == "includetest")
            {
                if (_includeSubTests)
                {
                    _markup = MarkupHelper.CreateLabel("&#x2193----------------------- INCLUDE SUB TEST ------" + TestStep + " -------------------&#x2193", ExtentColor.Blue);
                    _log.Info(_markup);
                }
                else
                {
                    _markup = MarkupHelper.CreateLabel("Sub Tests are not included because INCLUDE SUB TESTS option is unchecked", ExtentColor.Grey);
                    _log.Info(_markup);
                }              
            }
            else if (Keyword == "endincludetest")
            {
                _markup = MarkupHelper.CreateLabel("&#x2191----------------------- END OF SUB TEST -------" + TestStep + " -------------------&#x2191", ExtentColor.Blue);
                _log.Info(_markup);
            }
            else
            {
                _log.Pass("["+ _actionDuration + "]" + "  -  " + TestStep);

                if (!string.IsNullOrEmpty(ReportScreenShot))
                {
                    var AttachScreenShot = MediaEntityBuilder.CreateScreenCaptureFromPath(ReportScreenShot).Build();
                    _log.Info(ReportScreenShot, AttachScreenShot);
                }
            }            
        }

        public void LogAsBreakOnFail()
        {
            _markup = MarkupHelper.CreateLabel("[" + _actionDuration + "]" + "  -  " + TestStep, ExtentColor.Red);
            _log.Fatal(_markup);
            if (!string.IsNullOrEmpty(ReportScreenShot))
            {
                var AttachScreenShot = MediaEntityBuilder.CreateScreenCaptureFromPath(ReportScreenShot).Build();
                _log.Info(ActionExceptionMessage, AttachScreenShot);
            }
            _log.Info("Further steps are not executed or reported due to \"Break on Fail\" step is set to TRUE");
        }

        public void LogAsFail()
        {
            _markup = MarkupHelper.CreateLabel("[" + _actionDuration + "]" + "  -  " + TestStep, ExtentColor.Red);
            _log.Fail(_markup);
            if (!string.IsNullOrEmpty(ReportScreenShot))
            {
                var AttachScreenShot = MediaEntityBuilder.CreateScreenCaptureFromPath(ReportScreenShot).Build();
                _log.Info(ActionExceptionMessage, AttachScreenShot);
            }
        }
    }
}
