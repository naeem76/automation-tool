﻿using AutomationApp.ExecutorModel;
using AventStack.ExtentReports;
using AventStack.ExtentReports.MarkupUtils;
using AventStack.ExtentReports.Reporter;
using PuppeteerSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AutomationApp
{
    public delegate void callback(string _browser, string _status, int _current, int _max);
    public class TestDirectory
    {
        private const string FILETYPE = "*.xlsx";
        private const string TESTREPORTSFOLDERNAME = "TestReports";
        private const string TESTREPORTNAMEINFIX = "TestReport";
        private const string TESTREPORTTYPE = ".html";
        private const string LOGGERCONFIGFILE = "loggerConfig.xml";
        private const string DATETIMEFORMAT = "MM_dd_yyyy_hh_mm_ss";

        private string _directoryPath;
        private string _environmentPrefix;
        private bool _includeSubTests;
        public string CurrentTestReport { get; private set; }
        public List<string> TestReports = new List<string>();
        public DirectoryInfo _testReportsFolder { get; private set; }
        private List<TestFile> _testFiles = new List<TestFile>();       
        private ExtentReports _logger;
        private ExtentTest _log;
        public static ExtentHtmlReporter logReporter;
        private string _projectPath = Directory.GetCurrentDirectory();

        public event EventHandler<StatusEventArgs> StatusChanged;
        public bool _debugMode;
        public bool _screenshotOld;
        

        public TestDirectory(string path, string environmentPrefix, bool includeSubTests, bool debugMode, bool screenshotOld)
        {
            _directoryPath = path;
            _environmentPrefix = environmentPrefix;
            _includeSubTests = includeSubTests;
            _debugMode = debugMode;
            _screenshotOld = screenshotOld;
        }

        //Loads all files from the given directory path by user.
        public void LoadFiles()
        {
            var currentDirectory = Directory.GetFiles(@_directoryPath, FILETYPE).Where(exclude => !Path.GetFileName(exclude).StartsWith("~$")).ToList();
            if (!currentDirectory.Count.Equals(0))            
            {
                foreach (string file in currentDirectory)
                {                                     
                   _testFiles.Add(new TestFile(file, _environmentPrefix, _includeSubTests, updateStatus));
                }
            }
            else
            {
                throw new DirectoryNotFoundException();
            }
        }

        //Executes collection of files seperately.
        public async Task Run(string browserToTest, string environmentLabel, string author, string reportingPath)
        {
            killProcesses();
            

            //Initiate and configure the test reporter
            _testReportsFolder = Directory.CreateDirectory(Path.Combine(reportingPath, TESTREPORTSFOLDERNAME));
            string ExecTime = DateTime.Now.ToString(DATETIMEFORMAT);
            string DirectoryName = new DirectoryInfo(_directoryPath).Name;
            DirectoryInfo CurrentReportFolder = Directory.CreateDirectory(Path.Combine(_testReportsFolder.FullName, TESTREPORTNAMEINFIX + "_" + ExecTime + "_" + environmentLabel + "_" + browserToTest.ToUpper()));
            string testReportName = TESTREPORTNAMEINFIX + "_" + ExecTime + "_" + environmentLabel + "_" + browserToTest + TESTREPORTTYPE;
            CurrentTestReport = Path.Combine(CurrentReportFolder.FullName, testReportName);
            TestReports.Add(CurrentTestReport);

            //Sets the test's config and driver to run. 
            TestDriver testdriver = new TestDriver();
            var webDriver = testdriver.SetTest(browserToTest, _environmentPrefix, environmentLabel);

            //Puppeteer browser for screenshots
            Browser screenshotBrowser = null;
            if (!_debugMode && !_screenshotOld)
            {
                screenshotBrowser = await Task.Run(async () => await Puppeteer.LaunchAsync(new LaunchOptions
                {
                    Headless = true,
                    ExecutablePath = Environment.CurrentDirectory + "\\chrome-win32\\chromeP.exe"
                }));
            }

            var actionExecutor = new ActionExecutor(webDriver, CurrentReportFolder.FullName, environmentLabel, browserToTest, screenshotBrowser, _debugMode);

            //Initates the error logging api (Extent reporting system)
            logReporter = new ExtentHtmlReporter(CurrentTestReport);
            logReporter.LoadConfig(Path.Combine(@_projectPath, LOGGERCONFIGFILE));

            //Explicit configuration of logging system 
            _logger = new ExtentReports();
            _logger.AttachReporter(logReporter);
            _logger.AddSystemInfo("Browser", browserToTest);
            _logger.AddSystemInfo("Specific Environment", environmentLabel);
            _logger.AddSystemInfo("Test Executed By", author);
            _logger.AddSystemInfo("Executed Test Folder", DirectoryName);


            //"#test-view > div.subview-right.left > div > div.test-content > div.test-steps > table > tbody > tr > td:nth-child(2){display: none}" +
            //"#test-view > div.subview-right.left > div > div.test-content > div.test-steps > table > thead > tr > th:nth-child(2){display: none}" +
            string cssStyle = 
                              "tr.log > td:nth-child(2){width: 0px}" +
                              "#test-view > div.subview-right.left > div > div.test-content > div.test-steps > table > tbody > tr.log > td:nth-child(2) > td.timestamp{width: 0px !important}" +
                              "#test-view > div.subview-right.left > div > div.test-content > div.test-steps > table > thead > tr > th:nth-child(3){padding-left: 60px}" +
                              "#test-view > div.subview-right.left > div > div.test-content > div.test-steps > table > tbody > tr > td:nth-child(3){padding-left: 60px}";

            //string jStyle = "$('.test').click(function(){ $('.search-div').datepicker(); });";
            //string jsStyle = "$('.search-div').click(function(){ $('.search-div').datepicker(); });";
            //string j = "$('#controls > div').append('<button>new div</button>').click(function(){ $('.search-tests').datepicker('show'); });";
            //string j1 = "$('#controls > div > button').click(function(){ $('.search-div').datepicker(); });";
            logReporter.Configuration().CSS = cssStyle;
            //logReporter.Configuration().JS = j;
            //logReporter.Configuration().JS = j1;

            foreach (var testFile in _testFiles)
            {
                var test = Path.GetFileNameWithoutExtension(testFile.FilePath);
                _log = _logger.CreateTest(test, "");

                if (testFile.FileIsValid)
                {                 
                    //Execute the test file with corresponding actions
                    testFile.RunTest(actionExecutor, _log);
                }
                else
                {
                    _log.Fatal("Please verify the file contents and make sure all the test files are closed before running.");
                    _log.Log(Status.Error, testFile.Message);
                }
            }

            testdriver.EndTest();
            if(screenshotBrowser!=null)await screenshotBrowser.CloseAsync();
            
            _logger.Flush();
        }

        private void updateStatus(string _browser, string _status, int _current, int _max)
        {
            try
            {
                if (StatusChanged != null)
                {
                    StatusChanged(this, new StatusEventArgs() { browser = _browser, status = _status, current = _current, max = _max });
                }
            }
            catch(Exception e)
            {

            }

        }

        private void killProcesses()
        {
            var tokill = new List<string>() { "chromeP", "chromedriver", "geckodriver", "phantomjs" };

            foreach (string process in tokill)
            {
                try
                {
                    while(Process.GetProcesses().Where(p => p.ProcessName == process).FirstOrDefault() != null)
                        Process.GetProcesses().Where(p => p.ProcessName == process).FirstOrDefault().Kill();
                }
                catch
                {
                    Console.WriteLine("Error ending process " + process);
                }
            }
            
        }
    }

    public class StatusEventArgs : EventArgs
    {
        public string browser;
        public string status;
        public int current;
        public int max;
    }
}
