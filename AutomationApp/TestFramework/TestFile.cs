﻿using AutomationApp.DataGenerator;
using AutomationApp.ExecutorModel;
using AventStack.ExtentReports;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AutomationApp
{
    public class TestFile
    {
        private const string SHEETNAME = "Sheet1";
        private const string TESTSTEP = "teststeps";
        private const string KEYWORDCOLUMN = "keyword";
        private const string LOCATORTYPECOLUMN = "locatortype";
        private const string LOCATORVALUECOLUMN = "locatorvalue";
        private const string VALUECOLUMN = "value";
        private const string BREAKONFAILCOLUMN = "breakonfail";
        private const string FILETYPE = ".xlsx";

        //******************************************Constants for keywords. Must Include all the keywords required for test execution from the Excel or Data File************************************** 
        private string[] validKeywords = { "clicklink", "click", "navigate", "insertvalue",
                                           "verifypagetitle", "goback", "clearvalue", "screenshot",
                                           "includetest", "verifyexists", "verifylinkurl", "resize",
                                           "comment", "verifytext", "deletecookies", "verifypageurl",
                                           "pagedown", "pageup", "scrolltobottom", "scrolltotop",
                                           "testcase", "verifyplaceholder", "close", "verifyvisible", 
                                           "verifynotexists", "verifyenabled", "verifyselected", "maximize",
                                            "select", "verifydisabled", "wait", "fullscreenshot", "mousehover"};
        public string Message { get; private set; }
        public bool FileIsValid { get; private set; }
        public bool UsingSubFile { get; set; }
        public string FilePath { get; private set; }

        public callback updateStatus;

        public TestFile(string filePath, string environmentPrefix, bool includeSubTests, callback d)
        {
            FilePath = filePath;
            FileIsValid = false;
            updateStatus = d;
            LoadFile(FilePath, environmentPrefix, includeSubTests);
            
        }

        private List<TestAction> _testActions = new List<TestAction>();

        //Loads the current file contents and stores in _testActions for further execution.
        public void LoadFile(string filePath, string EnvironmentPrefix, bool includeSubTests)
        {
            FileIsValid = true;
            var rowNumber = 1;
            bool breakOnFail;

            try
            {
                //Gets the data from excel, parses and adds into a List of data.
                ReadExcelData dataFile = new ReadExcelData();
                var dataFromFile = dataFile.ExcelToDataTable(filePath, SHEETNAME);
                dataFile.PopulateInCollection(filePath, SHEETNAME);
                
                while (rowNumber++ <= dataFromFile.Rows.Count)
                {
                    Console.WriteLine(rowNumber);
                    var keyword = dataFile.ReadTestStep(rowNumber, KEYWORDCOLUMN);
                    var locatorType = dataFile.ReadTestStep(rowNumber, LOCATORTYPECOLUMN);
                    var locatorValue = dataFile.ReadTestStep(rowNumber, LOCATORVALUECOLUMN);
                    var value = dataFile.ReadTestStep(rowNumber, VALUECOLUMN);
                    var testStep = dataFile.ReadTestStep(rowNumber, TESTSTEP);
                    bool.TryParse(dataFile.ReadTestStep(rowNumber, BREAKONFAILCOLUMN), out breakOnFail);         

                    // Keyword Validation
                    var isValid = ! string.IsNullOrWhiteSpace(keyword);

                    if (isValid == true && validKeywords.Contains(keyword.ToLower()))
                    {
                        if (!(keyword.ToLower().Equals("comment")))
                        {
                            if (keyword.ToLower().Equals("navigate") || keyword.ToLower().Equals("verifypageurl") || keyword.ToLower().Equals("verifylinkurl"))
                            {
                                if (EnvironmentPrefix.Contains(value) && value != "/")
                                {
                                    value = EnvironmentPrefix;
                                }
                                else if (!(value.Contains("http://") || value.Contains("https://")))
                                {
                                    value = EnvironmentPrefix + value;
                                }
                            }

                                _testActions.Add(new TestAction(keyword, locatorType, locatorValue, value, testStep, breakOnFail, includeSubTests));                           

                            if (keyword.ToLower().Equals("includetest") && includeSubTests)
                            {
                                if(value!="")
                                {
                                    UsingSubFile = true;
                                    var SubFilePath = Path.Combine(Path.GetDirectoryName(FilePath), value + FILETYPE);
                                    LoadFile(SubFilePath, EnvironmentPrefix, includeSubTests);
                                    _testActions.Add(new TestAction("endincludetest", "", "", "", "" + testStep, breakOnFail, includeSubTests));
                                }
                            }
                        }
                    }
                    else
                    {
                        FileIsValid = false;
                        Message = string.Format("File is invalid => {0}. Error due to line {1} and in \"Keyword\" Column. Suggested lookup value for \"{2}\"", filePath, rowNumber, keyword);
                        break;
                    }
                }
            }
            catch(Exception fileExc)
            {
                throw fileExc;
            }
        }

        //Triggers the actions to be performed based on data collected from file.
        public bool RunTest(ActionExecutor actionExecutor, ExtentTest log)
        {
            int count = 0;
            foreach (var testAction in _testActions)
            {
                Console.WriteLine(count + "/" + _testActions.Count);
                if (testAction.RunAction(actionExecutor, log))
                {
                    return false;
                }

                /*
                if (testAction.ActionExceptionMessage != "")
                {
                    if(testAction.Keyword.ToLower()=="click" || testAction.Keyword.ToLower()=="verifyexists") System.Diagnostics.Debugger.Break();

                }
                */

                updateStatus(actionExecutor._browser,"Running", count, _testActions.Count);
                count++;
            }
            updateStatus(actionExecutor._browser, "Finished", 0, 0);
            return true;
        }
    }
}
