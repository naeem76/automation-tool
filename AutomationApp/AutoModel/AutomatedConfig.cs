﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationApp.AutoModel
{
    public class AutomatedConfig
    {
        public bool activated;
        public DateTime time;
        public string scriptsFolder;
        public int browserIndex;
        public bool oldscreenshotter;
        public bool debugmode;
    }
}
