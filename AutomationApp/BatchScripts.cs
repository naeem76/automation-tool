﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutomationApp
{
    public partial class BatchScripts : Form
    {
        public string selFolder;
        public List<ScriptData> allData;

        public BatchScripts(string _folder)
        {
            InitializeComponent();
            selFolder = _folder;
            allData = new List<ScriptData>();

            foreach (var folder in Directory.GetDirectories(selFolder))
            {
                var files = Directory.GetFiles(folder).Select(x => Path.GetFileName(x)).ToList();

                var envFile = Path.Combine(folder, "EnvironmentConfig.txt");
                var envFileContents = File.ReadAllLines(envFile);
                if (!envFileContents[0].StartsWith("AUTOMATE"))
                {
                    //noenvautomate.Add(new DirectoryInfo(folder).Name);
                    File.WriteAllText(envFile, "AUTOMATE=0" + Environment.NewLine);
                    File.AppendAllLines(envFile, envFileContents);
                    envFileContents = File.ReadAllLines(envFile);
                }

                allData.Add(new ScriptData()
                {
                    folderName = new DirectoryInfo(folder).Name,
                    envFile = envFile,
                    environments = envFileContents.Where(x => !x.StartsWith("AUTOMATE")).Select(x=> x.Split('=')[0]).ToList(),
                    selectedIndex = Convert.ToInt32(envFileContents.First(x => x.StartsWith("AUTOMATE")).Split('=')[1])
                });
            }

            mainList.DataSource = allData;

            mainList.SelectionMode = SelectionMode.One;
            mainList.SelectedIndexChanged += MainList_SelectedIndexChanged;
        }

        private void MainList_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnUpdate.Enabled = true;
            var item = ((ScriptData)mainList.SelectedItem);
            envBox.DataSource = item.environments;
            selectedLabel.Text = item.selectedIndex == 0 ? "No Environment Selected" : item.environments[item.selectedIndex-1];
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            var item = ((ScriptData)mainList.SelectedItem);

            var envFileContents = File.ReadAllLines(item.envFile);
            envFileContents= envFileContents.Where(x => !x.StartsWith("AUTOMATE")).ToArray();

            File.WriteAllText(item.envFile, "AUTOMATE=" + (envBox.SelectedIndex + 1) + Environment.NewLine);
            File.AppendAllLines(item.envFile, envFileContents);
            item.selectedIndex = (envBox.SelectedIndex + 1);

            selectedLabel.Text = item.selectedIndex == 0 ? "No Environment Selected" : item.environments[item.selectedIndex - 1];
        }
    }

    public class ScriptData
    {
        public string folderName;
        public string envFile;
        public List<string> environments;
        public int selectedIndex;

        public override string ToString()
        {
            return folderName;
        }
    }
}
