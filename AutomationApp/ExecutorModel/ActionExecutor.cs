﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using SeleniumExtras.WaitHelpers;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.Extensions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using PuppeteerSharp;

namespace AutomationApp.ExecutorModel
{
    
    public class ActionExecutor 
    {
        private const string TESTREPORTSFOLDERNAME = "TestReports";
        private const string DATETIMEFORMAT = "MMMM_dd_yyyy_hh_mm_ss";
        private const string SCREENSHOTTYPE = ".png";
        private const string SCREENSHOTSFOLDERNAME = "ScreenShots";

        private IWebDriver _driver;
        private IJavaScriptExecutor _jsdriver;
        private IWebElement _currentElement;
        private Browser _screenshotBrowser;
        //private Page _page;

        private Actions _seleniumAction;
        private string _env;
        public string _browser;
        private string _verificationMessage;
        private DirectoryInfo _screenShotFolder;
        private bool _debugMode;

        public ActionExecutor(IWebDriver driver, string currentReportingFolder, string env, string browser, Browser scr, bool debugMode)
        {
            _screenshotBrowser = scr ?? null;
            //_page = Task.Run(async () => await _screenshotBrowser.NewPageAsync()).Result;
            _debugMode = debugMode;


            _driver = driver;
            _env = env;
            _browser = browser;
            _jsdriver = (IJavaScriptExecutor)driver;
            _seleniumAction = new Actions(driver);
            _screenShotFolder = Directory.CreateDirectory(Path.Combine(currentReportingFolder, SCREENSHOTSFOLDERNAME));

        }

        //Return the type of element on the page.
        public By GetElementLocator(string locatorType, string locatorValue)
        {
            switch (locatorType.ToLower())
            {
                case "class":
                    return By.ClassName(locatorValue);

                case "id":
                    return By.Id(locatorValue);

                case "name":
                    return By.Name(locatorValue);

                case "xpath":
                    return By.XPath(locatorValue);

                case "cssselector":
                    return By.CssSelector(locatorValue);

                case "linktext":
                    return By.LinkText(locatorValue);

                default:
                    throw new InvalidEnumArgumentException();     
            }
        }


        //To execute the selenium action on the specified element on the web page
        public ActionResult PerformAction(string keyword, string locatorType, string locatorValue, string value, string testStep)
        {

            var actionResult = new ActionResult
            {
                Screenshot = "",
                ActionStatus = false,
                ActionException = ""            
            };

            WebDriverWait wait;

            if(_debugMode) wait = new WebDriverWait(_driver, new TimeSpan(0, 0, 5));
            else wait = new WebDriverWait(_driver, new TimeSpan(0, 0, 20));

            try
            {
                switch (keyword.ToLower())
                {
                    case "navigate":
                        if(value.ToLower().Equals(_driver.Url.ToLower()))
                        {
                            actionResult.ActionStatus = true;
                            break;
                        }
                        else
                        { 
                            _driver.Navigate().GoToUrl(value);
                            wait.Until(w => _jsdriver.ExecuteScript("return document.readyState").ToString() == "complete");
                            //_driver.Navigate().GoToUrl("javascript:document.getElementById('overridelink').click()");

                            actionResult.ActionStatus = true;
                            // For interacting with iframes. Still needs development
                            //_driver.SwitchTo().Frame(0);
                            //_driver.FindElement(By.CssSelector(".ytp-large-play-button")).Click();

                            break;
                        }

                    case "click":

                        try
                        {
                            _currentElement = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(GetElementLocator(locatorType, locatorValue)));
                            _currentElement.Click();
                        }
                        catch
                        {
                            if(value!="NEGLIGIBLE")
                            {
                                _currentElement = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(GetElementLocator(locatorType, locatorValue)));
                                _jsdriver.ExecuteScript("arguments[0].click();", _currentElement);
                            }
                            
                        }

                        

                        wait.Until(w => _jsdriver.ExecuteScript("return document.readyState").ToString() == "complete");
                        _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

                        if (_driver.WindowHandles.Count.Equals(2))
                        {
                            _driver.SwitchTo().Window(_driver.WindowHandles.Last());
                        }

                        actionResult.ActionStatus = true; 
                        break;                      

                    case "close":
                        if (_driver.WindowHandles.Count.Equals(2))
                        {
                            _driver.SwitchTo().Window(_driver.WindowHandles.Last());
                            _driver.Close();
                            _driver.SwitchTo().Window(_driver.WindowHandles.First());
                            actionResult.ActionStatus = true;
                        }
                        else
                        {
                            _verificationMessage = "Action Failed. Only one page is opened.";
                            ActionFailed(actionResult, null, keyword, locatorType, locatorValue, value, testStep);
                        }                                         
                        break;

                    case "insertvalue":
                        _currentElement = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(GetElementLocator(locatorType, locatorValue)));
                        _currentElement.SendKeys(value);
                        actionResult.ActionStatus = true;
                        break;

                    case "verifypagetitle":
                        try
                        {
                            wait.Until((_driver) => { return _driver.Title.Contains(value); });
                            actionResult.ActionStatus = true;
                            break;
                        }
                        catch
                        {
                            if (_driver.Title.Contains(value))
                            {
                                actionResult.ActionStatus = true;
                            }
                            else
                            {
                                _verificationMessage = string.Format("Verification Failed. Values do not match. Current page Tab title ({0}), excel value ({1})", _driver.Title, value);
                                ActionFailed(actionResult, null, keyword, locatorType, locatorValue, value, testStep);
                            }
                            break;
                        }           

                    case "verifypageurl":
                        if (value.Contains("*"))
                        {

                            var uri = new Uri(_driver.Url);
                            _driver.Url = uri.GetLeftPart(UriPartial.Path);
                            value = value.Split('*')[0];
                        }



                        try
                        {
                            var mainurl = new Uri(_driver.Url).GetLeftPart(UriPartial.Authority);
                            if (!value.StartsWith("/en")) if(_driver.Url.ToString().Substring(mainurl.Length).StartsWith("/en")) value=value.Insert(mainurl.Length, "/en");

                            wait.Until((_driver) => { return _driver.Url.ToLower().Contains(value.ToLower()); });
                            actionResult.ActionStatus = true;
                            break;
                        }
                        catch
                        {

                            if (_driver.Url.ToLower().Contains(value.ToLower()))
                            {
                                actionResult.ActionStatus = true;
                            }
                            else
                            {
                                _verificationMessage = string.Format("Verification Failed. Values do not match(Case is ignored while comparing). Current page URL ({0}), excel value ({1})", _driver.Url, value);
                                ActionFailed(actionResult, null, keyword, locatorType, locatorValue, value, testStep);
                            }
                            break;
                        }                       

                    case "goback":
                        _driver.Navigate().Back();
                        wait.Until(w => _jsdriver.ExecuteScript("return document.readyState").ToString() == "complete");
                        actionResult.ActionStatus = true;
                        break;

                    case "clearvalue":
                        _currentElement = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(GetElementLocator(locatorType, locatorValue)));
                        _currentElement.Clear();
                        actionResult.ActionStatus = true;
                        break;

                    case "screenshot":                       
                        actionResult.Screenshot  =  ScreenShot(_screenShotFolder.FullName.ToString(), testStep);
                        actionResult.ActionStatus = true;
                        break;

                    case "fullscreenshot":
                        if (_debugMode)
                        {
                            actionResult.ActionStatus = true;
                            break;
                        }
                        actionResult.Screenshot = FullScreenShot(_screenShotFolder.FullName.ToString(),testStep);
                        actionResult.ActionStatus = true;
                        break;

                    case "includetest":
                        actionResult.ActionStatus = true;
                        break;

                    case "endincludetest":
                        actionResult.ActionStatus = true;
                        break;

                    case "verifyexists":
                        _currentElement = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(GetElementLocator(locatorType, locatorValue)));
                        Thread.Sleep(100);

                        if (!_currentElement.Displayed)
                        {
                            _jsdriver.ExecuteScript("arguments[0].scrollIntoView(true);", _currentElement);
                            _currentElement = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(GetElementLocator(locatorType, locatorValue)));
                        }

                        bool isElementExists = _currentElement.Displayed;

                       
                        if (value.Equals("not null", StringComparison.InvariantCultureIgnoreCase))
                        {
                            if (isElementExists)
                            {                 
                                actionResult.ActionStatus = true;
                            }
                            else
                            {
                                _verificationMessage = string.Format("Verification Failed because Current element is not present on page and excel value is set to ({0})", value);
                                ActionFailed(actionResult, null, keyword, locatorType, locatorValue, value, testStep);
                            }
                        }
                        else if (value.Equals("null", StringComparison.InvariantCultureIgnoreCase))
                        {
                            if (isElementExists)
                            {
                                _verificationMessage = string.Format("Verification Failed because Current element is present on page and excel value is set to ({0})", value);
                                ActionFailed(actionResult, null, keyword, locatorType, locatorValue, value, testStep);                               
                            }
                            else
                            {
                                actionResult.ActionStatus = true;
                            }
                        }
                        else
                        {
                            if (isElementExists)
                            {
                                actionResult.ActionStatus = true;
                            }
                            else
                            {
                                _verificationMessage = string.Format("Verification Failed. Current element does not exists on page");
                                ActionFailed(actionResult, null, keyword, locatorType, locatorValue, value, testStep);
                            }
                        }

                        break;

                    case "verifylinkurl":
                        _currentElement = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(GetElementLocator(locatorType, locatorValue)));
                        var LinkUrlValue = _currentElement.GetAttribute("href");

                        if (String.IsNullOrWhiteSpace(LinkUrlValue))
                        {
                            _verificationMessage = string.Format("Unable to get the requested value from page. PLease check the file contents. Link URL value do not match. Current URL ({0}) is empty or cannot retrieve, Excel Value ({1})", LinkUrlValue, value);
                            ActionFailed(actionResult, null, keyword, locatorType, locatorValue, value, testStep);
                        }
                        else
                        {
                            if (value.Contains("*"))
                            {
                                var uri = new Uri(LinkUrlValue);
                                LinkUrlValue = uri.GetLeftPart(UriPartial.Path);
                                value = value.Split('*')[0];
                            }

                            if (LinkUrlValue.ToLower().Equals(value.ToLower()))
                            {
                                actionResult.ActionStatus = true;
                            }
                            else
                            {
                                _verificationMessage = string.Format("Verification Failed. Link URL value do not match(Case is ignored while comparing). Current URL ({0}), Excel Value ({1})", LinkUrlValue, value);
                                ActionFailed(actionResult, null, keyword, locatorType, locatorValue, value, testStep);
                            }
                        }                          
                        break;

                    case "verifytext":
                        _currentElement = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(GetElementLocator(locatorType, locatorValue)));
                        var elementTextValue = _currentElement.Text;

                        if (String.IsNullOrWhiteSpace(elementTextValue))
                        {
                            _verificationMessage = string.Format("Unable to get the text of an element. Current element's text value do not match. Current Text value ({0}) is empty or null, Excel Value ({1})", elementTextValue, value);
                            ActionFailed(actionResult, null, keyword, locatorType, locatorValue, value, testStep);
                        }
                        else
                        {
                            if (value.Equals("not null", StringComparison.InvariantCultureIgnoreCase))
                            {
                                if (string.IsNullOrWhiteSpace(elementTextValue))
                                {
                                    _verificationMessage = string.Format("Verification Failed. Current element's text value do not match. Current Text value ({0}), Excel Value ({1})", elementTextValue, value);
                                    ActionFailed(actionResult, null, keyword, locatorType, locatorValue, value, testStep);
                                }
                                else
                                {
                                    actionResult.ActionStatus = true;
                                }
                            }
                            else if (value.Equals("null", StringComparison.InvariantCultureIgnoreCase) || value.Equals(""))
                            {
                                if (string.IsNullOrWhiteSpace(elementTextValue))
                                {
                                    actionResult.ActionStatus = true;
                                }
                                else
                                {
                                    _verificationMessage = string.Format("Verification Failed. Current element's text value do not match. Current Text value ({0}), Excel Value ({1})", elementTextValue, value);
                                    ActionFailed(actionResult, null, keyword, locatorType, locatorValue, value, testStep);
                                }
                            }
                            else
                            {
                                if (elementTextValue.Contains(value))
                                {
                                    actionResult.ActionStatus = true;
                                }
                                else
                                {
                                    _verificationMessage = string.Format("Verification Failed. Current element's text value do not match. Current Text value ({0}), Excel Value ({1})", elementTextValue, value);
                                    ActionFailed(actionResult, null, keyword, locatorType, locatorValue, value, testStep);
                                }
                            }
                        }                
                        break;

                    case "verifyplaceholder":
                        _currentElement = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(GetElementLocator(locatorType, locatorValue)));
                        var PlaceHolderValue = _currentElement.GetAttribute("placeholder");

                        if (String.IsNullOrWhiteSpace(PlaceHolderValue))
                        {
                            _verificationMessage = string.Format("Unable to get the placeholder value. Current PlaceholderValue ({0}) is null or empty, Excel Value ({1})", PlaceHolderValue, value);
                            ActionFailed(actionResult, null, keyword, locatorType, locatorValue, value, testStep);
                        }
                        else
                        {
                            if (PlaceHolderValue.Equals(value))
                            {
                                actionResult.ActionStatus = true;
                            }
                            else
                            {
                                _verificationMessage = string.Format("Verification Failed. Placeholder value do not match. Current PlaceholderValue ({0}), Excel Value ({1})", PlaceHolderValue, value);
                                ActionFailed(actionResult, null, keyword, locatorType, locatorValue, value, testStep);
                            }
                        }                       
                        break;

                    case "verifynotexists":
                        _currentElement = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(GetElementLocator(locatorType, locatorValue)));

                        if(string.IsNullOrWhiteSpace(value))
                        {
                            if (_currentElement.Size.IsEmpty)
                            {
                                actionResult.ActionStatus = true;
                            }
                            else
                            {
                                _verificationMessage = string.Format("Verification failed because Element is present");
                                ActionFailed(actionResult, null, keyword, locatorType, locatorValue, value, testStep);                               
                            }
                        }
                        else
                        {
                           var elementText = _currentElement.Text;

                            if(elementText == value)
                            {
                                _verificationMessage = string.Format("Verification failed because Element is present");
                                ActionFailed(actionResult, null, keyword, locatorType, locatorValue, value, testStep);                              
                            }
                            else
                            {
                                actionResult.ActionStatus = true;
                            }
                        }
                        break;

                    case "verifyvisible":
                        _currentElement = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(GetElementLocator(locatorType, locatorValue)));
                        IWebDriver remoteDriver = ((RemoteWebElement)_currentElement).WrappedDriver;

                        bool IsElementVisible = (bool)((IJavaScriptExecutor)remoteDriver).ExecuteScript(
                                "var element = arguments[0]," +
                                "  elemArea = element.getBoundingClientRect(), " +
                                "  xPosition = elemArea.left + elemArea.width / 2, " +
                                "  yPosition = elemArea.top + elemArea.height / 2, " +
                                "  e = document.elementFromPoint(xPosition, yPosition); " +
                                "  for (; e; e = e.parentElement) {  " +
                                "  if (e === element) " +
                                "    return true; " +
                                "} " +
                                "return false; "
                                , _currentElement);

                        if (IsElementVisible)
                        {
                            actionResult.ActionStatus = true;
                        }
                        else
                        {
                            _verificationMessage = string.Format("Verification Failed. Element is not visible or unable to find element");
                            ActionFailed(actionResult, null, keyword, locatorType, locatorValue, value, testStep);                          
                        }                       
                        break;

                    case "select":
                        _currentElement = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(GetElementLocator(locatorType, locatorValue)));
                        SelectElement selectElement = new SelectElement(_currentElement); 
                        selectElement.SelectByText(value);
                        actionResult.ActionStatus = true;
                        break;

                    case "verifyselected":
                        _currentElement = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(GetElementLocator(locatorType, locatorValue)));
                       
                        if (_currentElement.Selected || _currentElement.GetAttribute("class") == "active" || _currentElement.GetAttribute("class") == "is-active" || _currentElement.GetAttribute("class").Contains("active"))
                        {
                            actionResult.ActionStatus = true;
                        }
                        else 
                        {
                            if (_currentElement.TagName == "select")
                            {
                                SelectElement selectedElement = new SelectElement(_currentElement);

                                if (selectedElement.SelectedOption.Text.Equals(value))
                                {
                                    actionResult.ActionStatus = true;
                                }
                                else
                                {
                                    _verificationMessage = string.Format("Verification Failed. Prefered value is not selected or values do not match current value{0}, Excel value {1}", selectedElement.SelectedOption.Text, value);
                                    ActionFailed(actionResult, null, keyword, locatorType, locatorValue, value, testStep);
                                }
                            }
                            else
                            {
                                _verificationMessage = string.Format("Verification Failed. Element is not selected");
                                ActionFailed(actionResult, null, keyword, locatorType, locatorValue, value, testStep);
                            }                              
                        }                                         
                        break;

                    case "verifyenabled":
                        _currentElement = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(GetElementLocator(locatorType, locatorValue)));

                        if (_currentElement.Enabled)
                        {
                            actionResult.ActionStatus = true;
                        }
                        else
                        {
                            _verificationMessage = string.Format("Verification Failed. Element is disabled");
                            ActionFailed(actionResult, null, keyword, locatorType, locatorValue, value, testStep);  
                        }
                        break;

                    case "verifydisabled":
                        _currentElement = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(GetElementLocator(locatorType, locatorValue)));

                        if (_currentElement.Enabled)
                        {
                            if (_currentElement.GetAttribute("aria-disabled") == "true")
                            {
                                actionResult.ActionStatus = true;
                            }
                            else
                            {
                                _verificationMessage = string.Format("Verification Failed. Element is Enabled");
                                ActionFailed(actionResult, null, keyword, locatorType, locatorValue, value, testStep);
                            }                           
                        }
                        else
                        {
                            actionResult.ActionStatus = true;
                        }
                        break;

                    case "deletecookies":
                        wait.Until(w => _jsdriver.ExecuteScript("return document.readyState").ToString() == "complete");

                        var xxxxxxxxxx = _driver.Manage().Cookies.AllCookies;
                        _driver.Manage().Cookies.DeleteAllCookies();
                        var b = _driver.Manage().Cookies.AllCookies;
                        _driver.Navigate().Refresh();
                        var c = _driver.Manage().Cookies.AllCookies;
                        actionResult.ActionStatus = true;
                        break;

                    case "testcase":
                        actionResult.ActionStatus = true;
                        break;

                    case "pageup":
                        _jsdriver.ExecuteScript(string.Format("window.scrollBy(0, -{0})", GetScrollValue()), "");
                        actionResult.ActionStatus = true;
                        break;

                    case "pagedown":
                        _jsdriver.ExecuteScript(string.Format("window.scrollBy(0, {0})", GetScrollValue()), "");
                        actionResult.ActionStatus = true;
                        break;

                    case "scrolltotop":
                        _jsdriver.ExecuteScript("window.scroll(0, -250);");
                        actionResult.ActionStatus = true;
                        break;

                    case "scrolltobottom":
                        _jsdriver.ExecuteScript("window.scrollBy(0,document.body.scrollHeight)");
                        actionResult.ActionStatus = true;
                        break;

                    case "resize":
                        Size newWindowSize = new Size(1000, 1000);
                        _driver.Manage().Window.Size = newWindowSize;
                        actionResult.ActionStatus = true;
                        break;

                    case "maximize":
                        _driver.Manage().Window.Maximize();
                        _driver.SwitchTo().Window(_driver.CurrentWindowHandle);
                        actionResult.ActionStatus = true;
                        break;

                    case "wait":
                        int sleepTime = 0;                       
                        int.TryParse(value, out sleepTime);
                        TimeSpan time = TimeSpan.FromSeconds(sleepTime);
                        Thread.Sleep(Convert.ToInt32(time.TotalMilliseconds));
                        actionResult.ActionStatus = true;
                        break;

                    case "mousehover":
                        _seleniumAction = new Actions(_driver);
                        _currentElement = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(GetElementLocator(locatorType, locatorValue)));
                        MouseHover();
                        Thread.Sleep(2000);
                        actionResult.ActionStatus = true;
                        break;

                    default:
                        break;
                }
            }

            catch (WebDriverException driverExc)
            {
                if(driverExc.Message.Contains("Unexpected error. System.Net.WebException") || (driverExc.Message?.Contains("null response was thrown sending an HTTP request") ?? false) || (driverExc.InnerException?.Message?.Contains("Unable to connect to the remote server") ?? false))
                {
                    return null;
                }

                ActionFailed(actionResult, driverExc, keyword, locatorType, locatorValue, value, testStep);
            }

            catch (WebException)
            {
                return null;
            }

            catch(InvalidEnumArgumentException)
            {
                actionResult.ActionStatus = false;
                actionResult.Screenshot = ScreenShot(_screenShotFolder.FullName.ToString(), testStep);
                actionResult.ActionException = "Error Info : " + string.Format(" There are invalid contents in the test file. Verify the contents. Please check LocatorType- \" {0} \" or LocatorValue- \" {1} \".", locatorType, locatorValue);
            }

            catch (Exception exception)
            {
                ActionFailed(actionResult, exception, keyword, locatorType, locatorValue, value, testStep);
            }
            
            return actionResult;
        }

        private int GetScrollValue()
        {
            long totalwidth1 = (long)(_jsdriver).ExecuteScript("return document.body.offsetWidth");
            long totalHeight1 = (long)(_jsdriver).ExecuteScript("return document.body.offsetHeight");
            int totalWidth = (int)totalwidth1;
            int totalHeight = (int)totalHeight1;

            // Get the Size of the Viewport
            long viewportWidth1 = (long)(_jsdriver).ExecuteScript("return document.documentElement.clientWidth");
            long viewportHeight1 = (long)(_jsdriver).ExecuteScript("return window.innerHeight");
            int viewportWidth = (int)viewportWidth1;    
            int viewportHeight = (int)viewportHeight1;

            return viewportHeight;
        }

        private void ActionFailed(ActionResult actionResult, Exception actExc,string keyword, string locatorType, string locatorValue, string value, string testStep)
        {

            if (keyword != "verifyvisible" && _currentElement != null)
            {
                MouseHover();
            }
            
            try
            {
                actionResult.Screenshot = ScreenShot(_screenShotFolder.FullName.ToString(), testStep);
            }
            catch
            {
                actionResult.Screenshot = "Unable to capture screenshot";
            }
            
            actionResult.ActionStatus = false;
            actionResult.ActionException = "Error Info : " + (actExc?.Message == null ? _verificationMessage : actExc.Message + string.Format(" [ Unable to Locate element on web Page with Keyword ({3}). Please check LocatorType- \" {0} \" or LocatorValue- \" {1} \"  or Value- \" {2} \" ]. ", locatorType, locatorValue, value, keyword) + "Detailed Exception Info : " + actExc.InnerException?.Message?? "No More detail info has reported");
        }

        //To focus on the current page element to perform action
        private void MouseHover()
        {
            try
            {
                Point elementPosition = _currentElement.Location;
                int XCoordinate = elementPosition.X;
                int YCoordinate = elementPosition.Y;

                
                if (_driver.GetType().Name.Equals("FirefoxDriver"))
                {
                    _jsdriver.ExecuteScript("arguments[0].scrollIntoView(true);", _currentElement);
                }
                /*
                else
                {
                    _seleniumAction.MoveToElement(_currentElement).MoveByOffset(XCoordinate, YCoordinate).Build();
                    _seleniumAction.MoveToElement(_currentElement);
                    _seleniumAction.Perform();
                }
                */
                _seleniumAction.MoveToElement(_currentElement).MoveByOffset(XCoordinate, YCoordinate).Build();
                _seleniumAction.MoveToElement(_currentElement);
                _seleniumAction.Perform();
            }
            catch
            {
            }
        }

        //Captures the viewport screenshot and saves to specified location
        public string ScreenShot(string screenshotLocation, string screenShotName)
        {
            try
            {
                Screenshot capture = ((ITakesScreenshot)_driver).GetScreenshot();
                string ExecTime = DateTime.Now.ToString(DATETIMEFORMAT);
                string screenShotPath = Path.Combine(screenshotLocation, screenShotName + "_" + ExecTime + SCREENSHOTTYPE);

                capture.SaveAsFile(screenShotPath, ScreenshotImageFormat.Png);
                Thread.Sleep(1000);
                
                //Gets the relative path and sends to extent report for reference rendering.
                Uri capturedScreenshot = new Uri(screenShotPath);
                Uri folder = new Uri(screenshotLocation);
                string relativeScreenshotpath = Uri.UnescapeDataString(
                                          folder.MakeRelativeUri(capturedScreenshot)
                                          .ToString()
                                          .Replace('/', Path.DirectorySeparatorChar)
                                          );
                //return screenShotPath;
                return relativeScreenshotpath;
            }
            catch (Exception)
            {
                return null;
            }           
        }

        //Takes a Full page Screenshot of current webpage
        public string FullScreenShot(string screenshotLocation, string screenShotName)
        {
            int totalwidth = (int)(long)((IJavaScriptExecutor)_driver).ExecuteScript("return document.body.offsetWidth");
            int totalHeight = (int)(long)((IJavaScriptExecutor)_driver).ExecuteScript("return document.body.parentNode.scrollHeight");
            string ExecTime = DateTime.Now.ToString(DATETIMEFORMAT);
            string screenShotPath = Path.Combine(screenshotLocation, screenShotName + "_" + ExecTime + SCREENSHOTTYPE);
            //Takes full screen shot and save to specified relative path

            if(_screenshotBrowser==null)
            {
                var fullImage = Take(_driver.Url, new Size(totalwidth + 50, totalHeight));
                fullImage.Save(screenShotPath);
            }
            else
            {
                Task<string> myTask = Task.Run(async () => await Take2(_driver.Url, new Size(totalwidth + 50, totalHeight), screenShotPath));
                var c = myTask.Result;
            }


            //Gets the relative path and sends to extent report for reference rendering.
            Uri capturedScreenshot = new Uri(screenShotPath);
            Uri folder = new Uri(screenshotLocation);
            string relativeScreenshotpath = Uri.UnescapeDataString(
                                      folder.MakeRelativeUri(capturedScreenshot)
                                      .ToString()
                                      .Replace('/', Path.DirectorySeparatorChar)
                                      );
            return relativeScreenshotpath;
            //return ScreenShot(screenshotLocation, screenShotName);
        }

        public static Image Take(string url, Size size)
        {
            Image result = new Bitmap(size.Width, size.Height);
            try
            {
                Console.WriteLine("taking screenshot");
                var thread = new Thread(() =>
                {

                    using (var browser = new WebBrowser())
                    {
                        browser.ScrollBarsEnabled = false;
                        browser.AllowNavigation = true;
                        browser.Navigate(url);
                        browser.Width = size.Width;
                        browser.Height = size.Height;
                        browser.ScriptErrorsSuppressed = true;
                        browser.DocumentCompleted += (sender, args) =>
                        {
                            if (browser == null) throw new Exception("Sender should be browser");
                            if (browser.Document == null) throw new Exception("Document is missing");
                            if (browser.Document.Body == null) throw new Exception("Body is missing");

                            using (var bitmap = new Bitmap(browser.Width, browser.Height))
                            {
                                browser.DrawToBitmap(bitmap, new Rectangle(0, 0, browser.Width, browser.Height));
                                result = (Image)bitmap.Clone();
                            }
                        };

                        int count = 0;
                        while (browser.ReadyState != WebBrowserReadyState.Complete)
                        {
                            if (count > 25) browser.Refresh();
                            if (count > 40) break;
                            Application.DoEvents();
                            Thread.Sleep(1000);
                            Console.WriteLine("Delay " + count + " - Browser Load");
                            count++;
                        }

                        Application.DoEvents();
                    }
                });


                thread.SetApartmentState(ApartmentState.STA);
                thread.Start();
                thread.Join();


                return result;
            }
            catch
            {
                throw new Exception("Error while taking Screenshot");
            }
        }

        public async Task<string> Take2(string url, Size size, string path)
        {
            var pages = await _screenshotBrowser.PagesAsync();

            await pages[0].GoToAsync(url);
            await pages[0].SetViewportAsync(new ViewPortOptions
            {
                Width = size.Width,
                Height = size.Height + 100
            });
            await pages[0].ScreenshotAsync(path);
            return "complete";

        }
    }
}
