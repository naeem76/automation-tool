﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System;
using System.Drawing;
using System.IO;
using System.Reflection;

namespace AutomationApp
{
    public class TestDriver
    {
        private IWebDriver driver ;
        private bool runHeadless = false;

        public IWebDriver SetTest(string browserToTest, string envBaseURL, string env)
        {
            if (driver == null)
            {
                if(browserToTest.ToLower() == "headless-chrome")
                {
                    browserToTest = "chrome";
                    runHeadless = true;
                }

                switch (browserToTest.ToLower())
                {
                    case "chrome":
                        ChromeOptions chromeOptions = new ChromeOptions();
                        
                        if (runHeadless)
                        {
                            chromeOptions.AddArguments("--headless");
                        }

                        chromeOptions.AddArguments("--start-maximized");
                        chromeOptions.AddArguments("--disable-infobars");
                        
                        // chromeOptions.AddAdditionalCapability(CapabilityType.AcceptSslCertificates, true);
                        chromeOptions.AcceptInsecureCertificates = true;
                        //chromeOptions.ToCapabilities();      

                        //DesiredCapabilities handlSSLErr = DesiredCapabilities.Chrome();
                        //handlSSLErr.SetCapability(CapabilityType.AcceptSslCertificates, true);

                        driver = new ChromeDriver(chromeOptions);
                        driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
                        driver.Manage().Cookies.DeleteAllCookies();
                        driver.Manage().Window.Maximize();

                        if (runHeadless)
                        {
                            driver.Manage().Window.Size = new Size(1440, 900);
                        }

                        //Disabling location by default.
                        if (envBaseURL.ToLower().Contains("https"))
                        {
                            string baseURL = string.Format("chrome://settings/content/siteDetails?site={0}", envBaseURL);
                            driver.Navigate().GoToUrl(baseURL);
                            driver.FindElement(By.XPath("//body")).SendKeys(Keys.Tab);
                            driver.SwitchTo().ActiveElement().SendKeys(Keys.Tab);
                            //if (false)
                            //{
                            driver.SwitchTo().ActiveElement().SendKeys(Keys.Down);
                            //}
                            driver.SwitchTo().ActiveElement().SendKeys(Keys.Down);
                        }
                        break;

                    case "firefox":
                        FirefoxOptions ffOptions = new FirefoxOptions();
                        ffOptions.SetPreference("dom.webnotifications.enabled", false);
                        ffOptions.AcceptInsecureCertificates = true;
                      
                        ffOptions.SetPreference("geo.enabled", false);  

                        driver = new FirefoxDriver(ffOptions);
                        driver.Manage().Window.Maximize();
                        driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
                        driver.Manage().Cookies.DeleteAllCookies();
                        break;

                    case "internetexplorer":
                        InternetExplorerOptions ieOptions = new InternetExplorerOptions();
                        ieOptions.IntroduceInstabilityByIgnoringProtectedModeSettings = true;
                        ieOptions.EnsureCleanSession = true;
                        ieOptions.IgnoreZoomLevel = true;
                        //ieOptions.AcceptInsecureCertificates = true;
                        //ieOptions.EnablePersistentHover = true;
                        //ieOptions.EnableNativeEvents = true;

                        driver = new InternetExplorerDriver(ieOptions);                       
                        driver.Manage().Window.Maximize();
                        driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
                        driver.Manage().Cookies.DeleteAllCookies();
                        break;

                    case "microsoftedge":
                        EdgeOptions edgeOptions = new EdgeOptions();
                        edgeOptions.AcceptInsecureCertificates = true;
                       
                        driver = new EdgeDriver();
                        driver.Manage().Window.Maximize();
                        driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
                        driver.Manage().Cookies.DeleteAllCookies();
                        break;
                }
            }
            return driver;
        }

        public void EndTest()
        {
            if (driver != null)
            {
                driver.Quit();
            }
            else
            {
                driver.Close();
            }
        }
    }
}
