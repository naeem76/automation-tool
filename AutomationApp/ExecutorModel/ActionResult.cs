﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationApp
{
    //Gets and sets the values from actionExecutor class.
   public class ActionResult
   {
        public bool ActionStatus { get; set; }

        public string Screenshot { get; set; }

        public string ActionException { get; set; }


   }
}
