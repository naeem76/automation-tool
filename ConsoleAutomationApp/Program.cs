﻿using AutomationApp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleAutomationApp
{
    class Program
    {
        private const string ENVCONFIG = "EnvironmentConfig.txt";
        public static string[] envData;
        public static TestDirectory testDirectory;
        public static string[] validBrowser = { "chrome", "firefox", "internetexplorer", "microsoftedge", "phantomJS", "headless-chrome" };
        public static string directoryPath;
        public static string reportingPath;
        public static string envLabel; 
        public static string  browser;
        public static string envToTest = "";


        static void Main(string[] args)
        {
            Console.WriteLine("\n-----------------Test Automation App Initiated----------------\n");

            if (ValidateArguments(args))
            {
                Program consoleApp = new Program();
                consoleApp.ExecuteApp();
            }
            else
            {
                Console.WriteLine("     Error: There are no acceptable arguments to run this application. Please pass valid \"Directory Path\", \"Environment to Test\", \"Browser\" in same order as arguments.\n");
                Environment.Exit(0);
            }
        }

        private void ExecuteApp()
        {
            testDirectory = new TestDirectory(directoryPath, envToTest, true,false,false);

            //Loading the test scripts.
            try
            {
                testDirectory.LoadFiles();
                Console.WriteLine("     Info: Test Scripts loaded successfully.\n");
                Console.WriteLine("     Environment: " + envLabel + ", Environment-URL: " + envToTest + "\n");
            }
            catch (DirectoryNotFoundException dirExc)
            {
                Console.WriteLine("     Error: Directory has no test files. " + dirExc.Message + "\n");
            }
            catch (Exception fileExc)
            {
                Console.WriteLine("     Error: unable to load files. Please close all Test Files before running or Check the paths for test files.\n" + fileExc.Message + "\n");
            }

            //Executing the test scripts.
            try
            {
                Console.WriteLine("     Info: Executing Test scripts. . . . . . . . . . . . . .\n");
                testDirectory.Run(browser, envLabel, Environment.UserName, reportingPath);
            }
            catch (Exception ex)
            {
                Console.WriteLine("     Error: Failed to execute Test Files. Unable to connect to the server or Execution stopped unexpectedly. \n" + ex.Message);
            }

            // Test Report initialization.
            try
            {
                Process.Start(testDirectory.CurrentTestReport);
                Console.WriteLine("     Info: Execution has completed and Test Report Generated at: " + testDirectory.CurrentTestReport);

                if (Convert.ToBoolean(ConfigurationManager.AppSettings["DeleteReports"]))
                {
                    DeleteReports(ConfigurationManager.AppSettings["DeleteReportsRange"]);
                    Console.WriteLine("     Info: Old test reports has been deleted" + testDirectory.CurrentTestReport);
                }              
            }
            catch
            {
                Console.WriteLine("     Error: Unable to generate report.");
            }
        }

        private static bool ValidateArguments(string[] args)
        {
            if (args.Length == 3 || args.Length == 4)
            {
                directoryPath = args[0];
                envLabel = args[1];
                browser = args[2];

                //Defaulting to directory path, if required reporting (Results storing) path is not specified.
                if(args.Length == 4)
                {
                    reportingPath = args[3];
                }
                else
                {
                    reportingPath = directoryPath;
                }
                

                try
                {
                    envData = File.ReadAllLines(Path.Combine(directoryPath, ENVCONFIG));

                    foreach (string envLine in envData)
                    {
                        var envKey = envLine.Split('=')[0].Trim();
                        var envValue = envLine.Split('=')[1].Trim();

                        if (envKey.ToLower().Contains(envLabel.ToLower()))
                        {
                            envToTest = envValue;
                        }
                    }

                    if (string.IsNullOrEmpty(envToTest))
                    {
                        Console.WriteLine("     Error: Please Specify valid Environment. Environment arguments should be considered from EnvironmentConfig.txt file. \n");
                        return false;
                    }
                }
                catch
                {
                    Console.WriteLine("     Error: Directory is invalid. EnvironmentConfig file is not found in directory.\n");
                    return false;
                }             

                if (string.IsNullOrEmpty(directoryPath))
                {
                    Console.WriteLine("     Error: Directory path is empty. Please enter valid direcotry path. \n");
                    return false;
                }              
                if (!validBrowser.Contains(browser))
                {
                    Console.WriteLine("     Error: Please Specify valid BROWSER. \n");
                    return false;
                }
                
                return true;
            }
            else return false;
        }

        private void DeleteReports(string days)
        {
            var reports = new DirectoryInfo(testDirectory._testReportsFolder.FullName).GetDirectories().ToList();

            List<DirectoryInfo> reportsToDelete = new List<DirectoryInfo>();

            foreach (var report in reports)
            {
                string[] reportNameValues = report.Name.Split('_');
                DateTime reportDate = Convert.ToDateTime(reportNameValues[1] + "/" + reportNameValues[2] + "/" + reportNameValues[3]);

                if (reportDate < DateTime.Now.AddDays(Convert.ToInt32("-" + days)))
                {
                    reportsToDelete.Add(report);
                }
            }
                reportsToDelete.ForEach(r => Directory.Delete(r.FullName, true));            
        }

    }   
}
